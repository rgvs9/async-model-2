#!/bin/bash

python3 -m da.compiler src/coordinator.da
python3 -m da.compiler src/client.da
python3 -m da.compiler src/worker.da
python3 -m da.compiler src/database.da
python3 -m da.compiler src/main.da
rm -f main.py.log
python3 -m da -f --logfilelevel info src/main.py config/$1
