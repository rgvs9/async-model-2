************************************************************
IMPLEMENTATION OF ALGORITHM BASED ON MVCC-TSO
************************************************************

**-----------**
INTRODUCTION
**-----------**

* We have implemented a distributed coordinator based policy evaluation scheme using multiversion concurrency control with timestamp ordering (MVCC-TSO).
* We have used DistAgo 1.0.4 for our implementation.

**---------------**
MODULES &  DESIGN
**---------------**

* We have created five processes Master, Client, Coordinator, Worker and Database. The description of them is as follows:


1.) Config.txt :
----------------
* The config file contains the following:
1.) Number_of_clients
2.) Number_of_coordinators
3.) Number_of_workers_per_coordinator
4.) Policy_file_name
5.) DB_init_file
6.) static_pol_eval_file_name
6.) MindDB_latency
7.) MaxdDB_latency
8.) Random request 
8.)The workload of each client is specified in the following format:
<Client_number, Object_id_1, Object_id_2, Action, type>
where type specifies if it is a “read” or a “write” request.


2.) MASTER :
-----------
* The master reads from the config file the number of workers,coordinators,clients and other parameters listed up.
* It starts all other processes.

3.) Client:
-----------
* The client reads the config file, reads the requests that are associated with it(Based on a client number)  and starts a loop for those requests.
* It waits until it receives a response for one request and then it sends another request.
* The number of clients started are mentioned in the config file.
* It finds the coordinator by a hash function which finds the value based on the subject_id and the number of coordinators.
* Client sends request in two manner:
	1.) Explicit sequence of requests as mentioned in the config file
	2.) Random request generation if rand_flag is set in the config file.


4.) Coordinator:
----------------
	a.) Subject Coordinator:
		* First the request reaches a subject coordinator.
		* The subject coordinator gives a global unique id called "GID" to the policy request.
		* It has a tentative attribute list called S_R_Cache to have tentative updates.
		* It adds subj_attr_list(tentative update) to the policy.
		* It has a DB_cache, in which it flushes values before commiting them to the database.
		* It also maintains a Dep_dict for all the dependencies. In this, it adds, for every policy all the other policies it is 
		  dependent on.
		* It adds the attributes in the tentative attribute list and passes the new policy to the resource coordinator.
		* It receives the policy evaluation result from the worker and resource conflict result from resource coordinator, and accordingly
		  passes the result to the client.

	b.) Resource Coordinator:
		* It just updates values in the DB_cache as there are no tentative updates for resource.
		* Functionality for conflict detection present.
		
5.)Worker:
------------
* Worker receives request from the second coordinator to evaluate the policy and also has cached subject and resource attribute lists.
* On the receive of this request, worker reads values from the database, if values are not passed from the coordinator.
* It evaluates the policy. All the conditions for subject and resource have been parsed by a parser function evaluate_policy.
* The result of the policy evaluation is sent back to the two coordinators.


**--------------------------**
Instruction to Run the Code:
**--------------------------**

* Unzip the folder phase4_Gadi_Mittal in the directory containing DistAlgo source code or which supports DistAlgo Run.
* The folder contains the following:
	README.txt
	testing.txt
	config - containing all the config files
	src - All the source files
	logs - All the log files generated
	psuedocode - psuedo code used for implementation

 
* There is a run.sh script which contains all the commands to compile all the source files and run the master process.
* To run the master process, type the following into the terminal:

	
 	sh run.sh config_1.txt

**--------------------------**
MAIN FILES:
**--------------------------**

* Master file : src/main.da
* Client file: src/client.da
* Coordinator file: src/coordinator.da
* Worker file: src/worker.da
* Database Emulator: src/database.da
* Database Initialization file: db_init.xml
* Policy-Evaluation file: policy-example.xml
* Static-Policy-details: static_pol_eval_req.txt
* Test cases : testing.txt
* README.txt
* run.sh to run the code
* Config folder contains the configuration file for all the test-cases.


**--------------------------**
CONTRIBUTIONS:
**--------------------------**

* We divided the work equally between both of us since the start of the project.Understanding and laying out the design was
done together.
* Shared a common GIT repository though which all the coordination was done.
* After that, The modules implemented are as follows:

	Rahul: Policy files, Coordinator. Testing.txt
	Aakriti: Master, client, worker and database emulator, README.txt

* After that we sat together and designed all the testcases.
* Did stress testing together.
* Also, divided the testcases and generated different log files.


**-----------------------------**
CONSISTENCY WITH THE PSEUDOCODE:
**-----------------------------**

* Our code is fully consistent with the pseudocode submitted in phase 1.
* Please find the pseudo- code attached in the folder pseudo-code.









